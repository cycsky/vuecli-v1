import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/store';

Vue.use(Router)

const router = rootWindow.__VueRouter = new Router({
  routes: [
    //demo
    {
      path: '/test',
      name: 'test',
      component: r => require(['@/view/HelloWorld'], r)
    },
    //首页
    {
      path: '/',
      name: 'index',
      meta: {
        title: '首页',
        hideHeader: false,
        hideTabBar: false,
      },
      component: r => require(['@/view/index/index'], r),
    },
    //附近
    {
      path: '/nearby',
      name: 'nearby',
      meta: {
        title: '附近',
      },
      component: r => require(['@/view/nearby/index'], r)
    },
    //活动
    {
      path: '/activity',
      name: 'activity',
      meta: {
        title: '活动',
      },
      component: r => require(['@/view/activity/index'], r)
    },
    //我的
    {
      path: '/ucenter',
      name: 'ucenter',
      meta: {
        title: '我的',
      },
      component: r => require(['@/view/ucenter/index'], r)
    }
  ]
});

router.afterEach((to, from) => {
  store.commit('routeChanged', {
    prevRoute: from,
    currRoute: to,
  });
  store.commit('setTabBar', !to.meta.hideTabBar);
  store.commit('setHeader', !to.meta.hideHeader);
  store.commit('changeTitle', to.meta.title || '我的APP');
  store.commit('routerChange', router.options.routes.find(v => {
    return v && v.name === to.name;
  }));

});
export default router;
