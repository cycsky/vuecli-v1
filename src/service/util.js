/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
*/

/* 常用函数库 */

let init = {
    //全局变量配置
    config: {
        wait: 60, //验证码 等待秒数
    },
    //检测客户端环境: 安卓 iPhone iPad 微信 微博 Win Mac Unix
    // if(/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) //判断是在移动端
    browserInfo: {
        ua: navigator.userAgent.toLowerCase(),
        isAndroid: Boolean(navigator.userAgent.match(/android/ig)),
        isIphone: Boolean(navigator.userAgent.match(/iphone|ipod/ig)),
        isIpad: Boolean(navigator.userAgent.match(/ipad/ig)),
        isWeChat: Boolean(navigator.userAgent.match(/MicroMessenger/ig)),
        isWeiBo: Boolean(navigator.userAgent.match(/WeiBo/ig)),
        isWin : Boolean(navigator.userAgent.indexOf("Win") != -1),
        isMac : Boolean(navigator.userAgent.indexOf("Mac") != -1),
        isUnix : Boolean(navigator.userAgent.indexOf("X11") != -1)
    },
    //精确判断对象类型 Object.prototype.toString
    /*
       var is = {types: ["Array", "Boolean", "Date", "Number", "Object", "RegExp", "String", "Window", "HTMLDocument"]}
        for (var i = 0, c; c = is.types[i++];) {
            is[c] = (function (type) {
                    return function (obj) {
                        return Object.prototype.toString.call(obj) == "[object " + type + "]";
                    }
                })(c);
        }
        alert(is.Array(obj)); // true
        alert(is.Date(new Date)); // true
        alert(is.RegExp(/reg/ig)); // true
    */
    is: {
        //获取对象类型
        getClass: function (obj) {
            return Object.prototype.toString.call(obj).match(/^\[object\s(.*)\]$/)[1];
            //return Object.prototype.toString.call(obj).match(/\s([a-z|A-Z]+)/)[1].toLowerCase();

        },
        //判断对象类型
        Array: function (obj) {
            return Object.prototype.toString.call(obj) == "[object Array]";
        },
        Boolean: function (obj) {
            return Object.prototype.toString.call(obj) == "[object Boolean]";
        },
        Date: function (obj) {
            return Object.prototype.toString.call(obj) == "[object Date]";
        },
        Number: function (obj) {
            return Object.prototype.toString.call(obj) == "[object Number]";
        },
        Object: function (obj) {
            return Object.prototype.toString.call(obj) == "[object Object]";
        },
        RegExp: function (obj) {
            return Object.prototype.toString.call(obj) == "[object RegExp]";
        },
        String: function (obj) {
            return Object.prototype.toString.call(obj) == "[object String]";
        },
        Window: function (obj) {
            return Object.prototype.toString.call(obj) == "[object Window]";
        },
        HTMLDocument: function (obj) {
            return Object.prototype.toString.call(obj) == "[object HTMLDocument]";
        },
    },


    //缓存版本号
    version: Math.ceil(Math.random() * 10) + '.' + Math.random(),

    //获取url参数; use: geturl("参数名")
    getUrlParam: function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    },
    //时间戳转化成时间 timetrans(1531238400) 注：- 换成 / (移动端浏览器返回Nan)
	timetrans: function (date) {
		var date = new Date(date * 1000);//如果date为10位不需要乘1000
		var Y = date.getFullYear() + '-';
		var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
		var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
		var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
		var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
		var s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
		return Y + M + D + h + m + s;
	},
    //获取指定区间随机数 GetRandomNum(20,66); 注：取1-10的随机数 Math.ceil(Math.random() * 10)
    getRandomNum: function (Min, Max) {
        var Range = Max - Min;
        var Rand = Math.random();
        return (Min + Math.round(Rand * Range));
    },
    //时间戳+小于四位随机数 注：暂时创建uid
    createRandom: function () {
        var u = new Date().getTime().toString();
        var id = parseInt(10000*Math.random());
        return u+id;
    },

    //倒计时 天时分秒; use:创建<span id="countDown"></span>
    countTime: function () {
        //获取当前时间
        var date = new Date();
        var now = date.getTime();
        //设置截止时间
        var str = "2018/10/18 09:30:00";
        var endDate = new Date(str);
        var end = endDate.getTime();

        //时间差
        var leftTime = end - now;
        //定义变量 d,h,m,s保存倒计时的时间
        var d, h, m, s;
        if (leftTime >= 0) {
            d = Math.floor(leftTime / 1000 / 60 / 60 / 24);
            h = Math.floor(leftTime / 1000 / 60 / 60 % 24);
            m = Math.floor(leftTime / 1000 / 60 % 60);
            s = Math.floor(leftTime / 1000 % 60);
        }

        //将倒计时赋值到div中
        if (d === undefined) {
            $("#countDown").empty().append("计时结束");
            return;
        } else {
            $("#countDown").empty().append(d + "天" + h + "时" + m + "分" + s + "秒");
        }

        //递归每秒调用countTime方法，显示动态时间效果
        setTimeout(init.countTime, 1000);
    },

    //预加载 获取页面所有img标签图片加载; use:创建<span id="percent"></span>
    loadAllPic: function () {
        var imgs = document.getElementsByTagName("img");
        var len = 0;
        var percent = document.getElementById("percent");
        for (var i = 0; i < imgs.length; i++) {
            (function (i) {
                imgs[i].onload = function () {
                    len++;
                    percent.innerText = Math.round(len * 100 / imgs.length) + "%";
                }
            })(i);
        }
    },
    //预加载 指定图片路径提前加载   use:init.perloadPic(["a.png","b.png","c.png"])
    preloadPic: function (arrImagPath) {
        var images = [];
        for (var i = 0; i < arrImagPath.length; i++) {
            images[i] = new Image()
            images[i].src = arrImagPath[i]
        }
    },
    //懒加载
    lazyLoadPic: function () {

    },
    //有序加载
    orderLoadPic: function () {

    },
    //返回顶部
    goTop: function () {
        var timer = null;
        cancelAnimationFrame(timer);
        timer = requestAnimationFrame(function fn() {
            var top = document.body.scrollTop || document.documentElement.scrollTop;
            if (top > 0) {
                document.body.scrollTop = document.documentElement.scrollTop = top - 50;
                timer = requestAnimationFrame(fn);
            } else {
                cancelAnimationFrame(timer);
            }
        });
    },

    //表单相关验证：手机号 邮箱 身份证 中文 英文 数字 密码/规则/强度
    checkPhone: function (phone) {
        var num = /^1[34578]\d{9}$/;
        return num.test(phone);
    },

    checkEmail: function (email) {
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
        //var reg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        return reg.test(email);
    },

    checkIdCrad: function (card) {
        var id = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        return id.test(card);
    },

    checkChinese: function (txt) {
        var str = /^[\u4E00-\u9FA5]{1,6}$/;
        return str.test(txt);
    },
    checkEnglish: function (txt) {
        var en=/^[A-Za-z]+$/;
        return en.test(txt);
    },
    checkNumber:function (num) {
        var str = /^[0-9]+.?[0-9]*$/;
        return str.test(num);
    },

    checkPasswordStrength: function (target) {
        var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
        var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
        var enoughRegex = new RegExp("(?=.{6,}).*", "g");

        if (false == enoughRegex.test(target)) {
            //密码小于六位
            console.log("小于6位");
        } else if (strongRegex.test(target)) {
            //密码为八位及以上并且字母数字特殊字符三项都包括,强度最强
            console.log("强");
        } else if (mediumRegex.test(target)) {
            //密码为七位及以上并且字母、数字、特殊字符三项中有两项，强度是中等
            console.log("中");
        } else {
            //如果密码为6为及以下，就算字母、数字、特殊字符三项都包括，强度也是弱的
            console.log("弱");
        }
        return true;
    },

    //动态加载 CSS
    LoadStyle: function (path) {
        try {
            document.createStyleSheet(path);
        } catch (e) {
            var cssLink = document.createElement('link');
            cssLink.rel = 'stylesheet';
            cssLink.type = 'text/css';
            cssLink.href = path;
            var head = document.getElementsByTagName('head')[0];
            head.appendChild(cssLink)
        }
    },

    //动态加载 JS
    loadScript: function (path) {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.src = path;
        script.type = 'text/javascript';
        head.appendChild(script);
    },

    //设置 Cookie
    setCookie: function (name, value, Hours) {
        var d = new Date();
        var offset = 8;
        var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        var nd = utc + (3600000 * offset);
        var exp = new Date(nd);
        exp.setTime(exp.getTime() + Hours * 60 * 60 * 1000);
        document.cookie = name + "=" + escape(value) + ";path=/;expires=" + exp.toGMTString() + ";"
    },
    //获取 Cookie
    getCookie: function (name) {
        var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
        if (arr != null) return unescape(arr[2]);
        return null
    },
    //删除 Cookie
    delCookie: function (name) {
        init.setCookie(name, "", -1);
    },

    //N秒后跳转
    secondSkip: function (second, url, doc) {
        var s = second;
        setInterval(function () {
            if (s == 0) {
                window.location.href = url;
            }
            $(doc).empty().append(s);
            s--;
        }, 1000)

        //秒倒计时
	    /*
	    var timesRun = 3;
	    var interval = setInterval(function(){
		    console.log(timesRun);
		    timesRun--;
		    if(timesRun === 0){
			    clearInterval(interval);
		    }
		    //do whatever here..
	    }, 1000);
	    */
    },

    //短信验证码后60秒倒计时  注:不能防止页面刷新
    countdown: function (obj) {
        if (init.config.wait == 0) {
            $(obj).removeAttr("disabled");
            $(obj).val("获取验证码");
            init.config.wait = 60;
            return;
        } else {
            $(obj).attr("disabled", true);
            $(obj).val(init.config.wait + "s 重新发送");
            init.config.wait--;
        }
        setTimeout(function () {
            init.countdown(obj)
        }, 1000)
    },

    //短信验证码后60秒倒计时 基于cookie防刷新
    msgCountdown: function (snedBut, phoneNum) {

        //发送验证码时添加cookie
        function addCookie(name, value, expiresHours) {
            //判断是否设置过期时间,0代表关闭浏览器时失效
            if (expiresHours > 0) {
                var date = new Date();
                date.setTime(date.getTime() + expiresHours * 1000);
                init.setCookie(name, escape(value), {expires: date});
            } else {
                init.setCookie(name, escape(value));
            }
        }

        //修改cookie的值
        function editCookie(name, value, expiresHours) {
            if (expiresHours > 0) {
                var date = new Date();
                date.setTime(date.getTime() + expiresHours * 1000); //单位是毫秒
                init.setCookie(name, escape(value), {expires: date});
            } else {
                init.setCookie(name, escape(value));
            }
        }

        //获取cookie的值
        function getCookieValue(name) {
            return init.getCookie(name);
        }

        //发送验证码
        function sendCode(obj) {
            var phonenum = $(phoneNum).val();
            var result = init.checkPhone(phonenum);

            if (result) {
                // Ajax 请求成功后设置cookie缓存...
                addCookie("secondsremained", 5, 5);//添加cookie记录,有效时间60s
                setTime(obj);//开始倒计时
            }
        }

        //开始倒计时
        function setTime(obj) {
            var countdown = getCookieValue("secondsremained");

            if (countdown == 0) {
                obj.removeAttr("disabled");
                obj.val("免费获取验证码");
                return;
            } else {
                obj.attr("disabled", true);
                obj.val("重新发送(" + countdown + ")");
                countdown--;
                editCookie("secondsremained", countdown, countdown + 1);
            }
            setTimeout(function () {
                setTime(obj)
            }, 1000)

        }

        //cookie值大于0 继续倒计时 否则重新发送
        getCookieValue("secondsremained") > 0 ? setTime($(snedBut)) : sendCode($(snedBut));

    },
    //获取当前年月日时分秒星期
    getDateTime: function () {
        var date = new Date();
        this.year = date.getFullYear();
        this.month = date.getMonth() + 1;
        this.date = date.getDate();
        this.day = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六")[date.getDay()];
        this.hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        this.minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        this.second = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        var currentTime = "现在是:" + this.year + "年" + this.month + "月" + this.date + "日 " + this.hour + ":" + this.minute + ":" + this.second + " " + this.day;
        return currentTime;
    },
    //判断Body元素滚动到底部
    bodyScrollBottom: function () {
        $(window).scroll(function () {
            var scrollTop = $(this).scrollTop();
            var scrollHeight = $(document).height();
            var windowHeight = $(this).height();
            if (scrollTop + windowHeight == scrollHeight) {
                $.tipsMsg("Body滚动条到底部了");
            }
        });
    },
    //判断DIV元素滚动到底部
    docScrollBottom: function (target) {
        var nScrollHight = 0; //滚动距离总长(注意不是滚动条的长度)
        var nScrollTop = 0;   //滚动到的当前位置
        var nDivHight = $(target).height();
        $(target).scroll(function () {
            nScrollHight = $(this)[0].scrollHeight;
            nScrollTop = $(this)[0].scrollTop;
            if (nScrollTop + nDivHight >= nScrollHight) {
                $.tipsMsg("DIV滚动条到底部了");
            }
        });
    },

    //保留2位小数 千分位分隔 参数(单位,保留几位)  调用init.fmoney(9.7,2); 结果:9.70
    fmoney: function (s, n) {
        n = n > 0 && n <= 20 ? n : 2;
        s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
        var l = s.split(".")[0].split("").reverse(),
            r = s.split(".")[1];
        t = "";
        for (i = 0; i < l.length; i++) {
            t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
        }
        return t.split("").reverse().join("") + "." + r;
    },
    //还原函数 千分位
    rmoney: function (s) {
        return parseFloat(s.replace(/[^\d\.-]/g, ""));
    },
    //获取两个日期间隔天数
    getDateDiff: function (startDate, endDate) {
        var startTime = new Date(Date.parse(startDate.replace(/-/g, "/"))).getTime();
        var endTime = new Date(Date.parse(endDate.replace(/-/g, "/"))).getTime();
        var dates = Math.abs((startTime - endTime)) / (1000 * 60 * 60 * 24);
        return dates;
    },
    //获取当前日期几天前或几天后
    getDateBeforeAfter: function (days) {
        var now = new Date();
        if (days >= 1) {
            now = new Date(now.getTime() - 86400000 * days);
        }
        var yyyy = now.getFullYear(), mm = (now.getMonth() + 1).toString(), dd = now.getDate().toString();
        if (mm.length == 1) {
            mm = '0' + mm;
        }
        if (dd.length == 1) {
            dd = '0' + dd;
        }
        return (yyyy + '-' + mm + '-' + dd);
    },
    //canvas生成新base64图片
    canvasCreateImg: function (width,height) {
        var c = document.createElement("canvas");
        var ctx = c.getContext("2d");
        c.width = width;
        c.height = height;
        ctx.fillStyle = "#ffffff";
        ctx.fillRect(0, 0, 750, 1334);
        ctx.drawImage(document.getElementById("canvasImg"), 0, 0, width, height);
        console.log(c.toDataURL("image/jpeg",0.5)); //图片品质 0-1

    },
    //解决移动端Android软键盘遮住输入框
	isAndroid: function () {
		if (/Android [4-6]/.test(navigator.appVersion)) {
			window.addEventListener("resize", function () {
				if (document.activeElement.tagName == "INPUT" || document.activeElement.tagName == "TEXTAREA") {
					window.setTimeout(function () {
						document.activeElement.scrollIntoViewIfNeeded();
					}, 0);
				}
			})
		}
	}

}

export default init;
