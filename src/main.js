// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store';
import {post, get, patch, put} from './service/http'
import init from './service/util';
import MintUI from 'mint-ui'

import 'mint-ui/lib/style.css'

Vue.use(MintUI)

// 定义全局变量 ajax
Vue.prototype.$post = post
Vue.prototype.$get = get
Vue.prototype.$patch = patch
Vue.prototype.$put = put

Vue.prototype.util = init

//开发环境提示
Vue.config.productionTip = false

//区分生产与开发环境
process.env.NODE_ENV == 'production';
process.env.NODE_ENV == 'development';


/* eslint-disable no-new */
new Vue({
  template: '<App/>',
  render: h => h(App),
  store,
  router
}).$mount('#app');
