import Mock from 'mockjs'
const Random = Mock.Random;
var root = process.env.API_ROOT;

export default Mock.mock(root + '/msg', 'post', function () {
  let articles = [];
  for (let i = 0; i < 100; i++) {
    let newArticleObject = {
      title: Random.csentence(5, 30),
      thumbnail_pic_s: Random.dataImage('300x250', 'mock的图片'),
      author_name: Random.cname(),
      date: Random.date() + ' ' + Random.time()
    }
    articles.push(newArticleObject)
  }
  return {
    articles: articles
  }
});
