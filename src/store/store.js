/*eslint-disable*/
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

let store = new Vuex.Store({
  state: {
    currRouter: null, // 当前路由

    //是否显示tabbar
    showBar: true,
    showHeader: true,
    prevRoute: {},
    currRoute: {},
    title: '我的WebApp', // 标题

  },

  mutations: {
    // 路由改变
    routerChange(state, op) {
      state.currRouter = op;
    },
    routeChanged(state, opt) {
      state.prevRoute = opt.prevRoute;
      state.currRoute = opt.currRoute;
    },
    setTabBar(state, showBar) {
      state.showBar = showBar;
    },
    setHeader(state, showHeader) {
      state.showHeader = showHeader;
    },
    changeTitle(state, title) {
      state.title = title;
    },

  },
  modules: {}

});

export default store;
