# vue webApp

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

> 技术参考文档

mint-ui: https://mint-ui.github.io/#!/zh-cn

Router: 
https://www.cnblogs.com/mengfangui/p/8081327.html
https://www.cnblogs.com/superlizhao/p/8527317.html

mockjs: http://mockjs.com/examples.html

vue: https://cn.vuejs.org/v2/api/index.html#ignoredElements

vueX: https://vuex.vuejs.org/zh/

